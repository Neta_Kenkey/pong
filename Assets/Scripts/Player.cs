﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float velocidadY;
    public float maxY;
    public float minY;
    private float posY;
    private float direction;

    // Update is called once per frame
    void Update()
    {

        direction = Input.GetAxis("Vertical");

        posY = transform.position.y + direction * velocidadY * Time.deltaTime;

        if (posY > maxY)
        {
            posY = maxY;
        }
        else if (posY < minY)
        {
            posY = minY;
        }

        transform.position = new Vector3(transform.position.x, posY, transform.position.z);
    }
}
